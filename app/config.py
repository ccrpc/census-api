import os

DB_INFO = {
    'host': os.environ.get('DB_HOST'),
    'port': os.environ.get('DB_PORT'),
    'dbname': os.environ.get('DB_NAME'),
    'user': os.environ.get('DB_USER'),
    'password': os.environ.get('DB_PASSWORD'),
}

APP_HOST = os.environ.get('APP_HOST')
APP_PORT = os.environ.get('APP_PORT')
DEBUG = os.environ.get('DEBUG')

PROJECTS = ['sierra', 'ameren']
