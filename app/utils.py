from config import DB_INFO
from psycopg2 import sql, connect
import psycopg2
import werkzeug
from flask import (
    jsonify
)


def get_table_columns(table_name):
    with connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(
                sql.SQL("Select * FROM census.{table} LIMIT 0").format(
                    table=sql.Identifier(table_name))
                )
            fields = [desc[0] for desc in curs.description if
                      desc[0] != 'geoid' and desc[0] != 'level']
    return fields


def query_database(query_str, data, fetch_type='single'):
    with connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            try:
                curs.execute(query_str, data)
            except psycopg2.errors.UndefinedColumn as e:
                raise werkzeug.exceptions.BadRequest(f'{e.pgerror}')

            if fetch_type == 'all':
                return curs.fetchall()
            else:
                res = curs.fetchone()
                if res[0]['features']:
                    return jsonify(res[0])
                else:
                    raise werkzeug.exceptions.NotFound(
                        'No features found, change the parameter and try again.')
