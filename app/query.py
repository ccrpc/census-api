from psycopg2 import sql


QUERY_IDENTITY = sql.SQL("""
    SELECT geoid, name, level FROM
    census.spatial
        WHERE ST_Contains(
            spatial.geom, ST_Transform(ST_SetSRID(
                ST_MakePoint(%s, %s), 4326), 3857))
""")

QUERY_GEOJSON = """
    SELECT jsonb_build_object(
            'type',     'FeatureCollection',
            'features', jsonb_agg(features.feature)
        )
        FROM (
        SELECT jsonb_build_object(
            'type',       'Feature',
            'id',         geoid,
            'geometry',   ST_AsGeoJSON(geom)::jsonb,
            'properties', to_jsonb(inputs) - 'geoid' - 'geom'
        ) AS feature
        FROM (
            %s
        ) inputs) features;
"""

QUERY_DETAIL_GEOID = sql.SQL(
    QUERY_GEOJSON % (
        """
        SELECT {table}.{fields}, {table}.geoid, {table}.level, ST_Transform(spatial.geom, 4326) geom
                FROM census.spatial
                LEFT JOIN census.{table} ON {table}.geoid = spatial.geoid
                WHERE spatial.geoid IN %s
        """
    )
)


QUERY_DETAIL_COOR = sql.SQL(
    QUERY_GEOJSON % (
        """
        SELECT {table}.{fields}, {table}.geoid, {table}.level, ST_Transform(spatial.geom, 4326) geom
        FROM census.spatial
        LEFT JOIN census.{table} ON {table}.geoid = spatial.geoid
            WHERE ST_Contains(
                spatial.geom, ST_Transform(ST_SetSRID(
                    ST_MakePoint(%s, %s), 4326), 3857))
        """
    )
)
