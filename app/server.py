from flask import (
    Flask,
    request,
    jsonify
)
from psycopg2 import sql, connect
from config import DB_INFO, APP_HOST, APP_PORT, DEBUG, PROJECTS
import query as q
import werkzeug
from utils import get_table_columns, query_database

# Create the application instance
app = Flask(__name__)


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(e):
    return {"message": e.description}, 400


@app.errorhandler(werkzeug.exceptions.NotFound)
def handle_not_found(e):
    return {'message': e.description}, 404


@app.route('/identity')
def get_identity():
    args = request.args
    try:
        lat = args['lat']
        lon = args['lon']
    except KeyError:
        raise werkzeug.exceptions.BadRequest('Please include lat and lon in the url parameter.')

    res = query_database(q.QUERY_IDENTITY, (lon, lat), fetch_type='all')
    return jsonify([{'geoid': row[0], 'name': row[1], 'level': row[2]}
                    for row in res])


@app.route('/detail')
def get_detail():
    args = request.args
    lat = args.get('lat')
    lon = args.get('lon')
    geoid = args.get('geoid')
    project = args.get('project')
    fields = args.get('field')

    if not ((lat and lon) and project) and not (geoid and project):
        projects = ','.join(list(PROJECTS.keys()))
        message = f"Please enter a project parameter with one of the follow value: {projects} and provide lat,lon or geoid."
        raise werkzeug.exceptions.BadRequest(message)

    if fields:
        fields = fields.split(',')
        fields = [field for field in fields
                  if field != 'geoid' and field != 'level']
    else:
        fields = get_table_columns(project)

    if lat and lon:
        query_str = q.QUERY_DETAIL_COOR.format(
            table=sql.Identifier(project),
            fields=sql.SQL(',').join(
                [sql.Identifier(field) for field in fields]
            )
        )
        data = (lon, lat)

    if geoid:
        geoids = geoid.split(',')
        query_str = q.QUERY_DETAIL_GEOID.format(
            table=sql.Identifier(project),
            fields=sql.SQL(',').join(
                [sql.Identifier(field) for field in fields]
            )
        )
        data = (tuple(geoids), )

    return query_database(query_str, data)


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host=APP_HOST, port=APP_PORT, debug=DEBUG)
