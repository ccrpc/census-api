# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.4.1](https://gitlab.com/ccrpc/census-api/compare/v1.4.0...v1.4.1) (2020-12-11)


### Bug Fixes

* **config:** corrected the census code for veterans ([e3819d5](https://gitlab.com/ccrpc/census-api/commit/e3819d56209d2b61a30ea7e6534b586cbf2c8afc))

## [1.4.0](https://gitlab.com/ccrpc/census-api/compare/v1.3.0...v1.4.0) (2020-12-04)


### Features

* **identity:** Add level to json result ([d31a94c](https://gitlab.com/ccrpc/census-api/commit/d31a94c3992a4ee559c57afabf5f1a36f397149e)), closes [#12](https://gitlab.com/ccrpc/census-api/issues/12)

## [1.3.0](https://gitlab.com/ccrpc/census-api/compare/v1.2.2...v1.3.0) (2020-12-03)


### Features

* **server:** Initial work on adding field to query parameter ([618fb7b](https://gitlab.com/ccrpc/census-api/commit/618fb7bbfef953c1114d27a6bbf45ca36738b27d))

### [1.2.2](https://gitlab.com/ccrpc/census-api/compare/v1.2.1...v1.2.2) (2020-12-01)


### Bug Fixes

* **query:** Return all fields from project tables ([88c80ba](https://gitlab.com/ccrpc/census-api/commit/88c80ba7f3751e1aec7fece9ce92b39ad35b1b56))

### [1.2.1](https://gitlab.com/ccrpc/census-api/compare/v1.2.0...v1.2.1) (2020-12-01)

## [1.2.0](https://gitlab.com/ccrpc/census-api/compare/v1.1.7...v1.2.0) (2020-12-01)


### Features

* **import:** Set up pre / post import db config ([21bd9ff](https://gitlab.com/ccrpc/census-api/commit/21bd9ffefa0070ba0ec1c1c41a95b9b241a93d40))

### [1.1.3](https://gitlab.com/ccrpc/census-api/compare/v1.1.2...v1.1.3) (2020-11-25)


### Bug Fixes

* **build:** Add requests library ([2cdc3bd](https://gitlab.com/ccrpc/census-api/commit/2cdc3bd2a75a6f211011fe671fd90660beea8b11)), closes [#9](https://gitlab.com/ccrpc/census-api/issues/9)

### [1.1.2](https://gitlab.com/ccrpc/census-api/compare/v1.1.1...v1.1.2) (2020-11-25)


### Bug Fixes

* **import:** Fix f string template ([48801a9](https://gitlab.com/ccrpc/census-api/commit/48801a97d8acb585eea3fb876329ebb24f6da7c8)), closes [#8](https://gitlab.com/ccrpc/census-api/issues/8)

### [1.0.1](https://gitlab.com/ccrpc/census-api/compare/v1.0.0...v1.0.1) (2020-11-24)

## 1.0.0 (2020-11-20)


### Features

* **docker kubernetes:** intermediate work ([eeff3b8](https://gitlab.com/ccrpc/spatial-microservice/commit/eeff3b8ab4133b078dcd0dd71de9ced44bf56507))
* **query:** Transform GeoJSON output to 4326 ([3bc01de](https://gitlab.com/ccrpc/spatial-microservice/commit/3bc01deeffad7432418513cb3a2d60b186cb57fa))
* **server:** Add better error handling. ([19947e7](https://gitlab.com/ccrpc/spatial-microservice/commit/19947e779710e16b0de49f93ea276511b7b6d6a6))
* **server:** Add detail and identify end points ([9288f57](https://gitlab.com/ccrpc/spatial-microservice/commit/9288f57a66cb01ae62767caaeeb4498b62fc75c9))
* **server:** Add wsgi entry point script ([749f4c3](https://gitlab.com/ccrpc/spatial-microservice/commit/749f4c3b169f05172b5c6bf88d65fd86defb6895))
* **sever:** Add query support to generate GeoJSON file. ([43c1fb6](https://gitlab.com/ccrpc/spatial-microservice/commit/43c1fb6c3151adab63571532971915858522452e))
* **spatial microservice:** tried to build custom orm ([a758dd6](https://gitlab.com/ccrpc/spatial-microservice/commit/a758dd6c173b2d0cc847144d20776efc397b8442))
