FROM python:3.8-alpine

ADD app /var/www/app
WORKDIR /var/www/app
ENV STATIC_URL /geocode
ENV STATIC_PATH /var/www/app/geocode

RUN apk update && apk add --no-cache\
  bash \
  nano \
  postgresql-dev \
  gcc \ 
  python3-dev \
  musl-dev 

EXPOSE 5000

COPY ./requirements.txt /var/www/requirements.txt
RUN pip3 install --no-cache-dir -r /var/www/requirements.txt

CMD ["gunicorn", "--bind", ":5000", "wsgi:app"]
