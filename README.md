# Census import tool

This is a tool for interacting with the US Census's API to download data

This project also has an app component, though the details on what that does are currently unknown.

The main current function of this project revolves round the scripts in the import folder which:

- Download Demographic Data for the Zip Code Tabulation Areas of the entire country (as we have not found a way to filter by state)
  - Saves this information in a csv file in the import/output folder
  - Uploads this information to our main postgis database.
- Downloads Other Spatial Data (Census Tracts, Block Groups, and County)
  - Saves this information in a geopackage file in import/output
  - Uploads this information to our main postgis database.
- Downloads Demographic data for the state of Illinois, specifically the first 203 counties in Illinois.
  - Uploads this information to our main postgis database.

The scripts are currently set up to fetch information for the Ameren project (https://gitlab.com/ccrpc/ameren.tracking/) though that can be changed
by editing import/import.sh and import/config.py.

To add a new project for which data is downloaded one will want to add a configuration class in import/config.py and add a line to import/import.sh
following the form `python3 /opt/code/census-download/import/census_data_download.py NEWPROJECNAME`.

Effectively the config.py holds configuration options which import.sh passes along to the download scripts.

Sierra is currently set up as another potential configuration though it has not been tested with the newest layout of this project.

## To run the scripts

Go to the infra folder and copy `census-secret.example.yaml` to `census-secret.yaml`.

In `census-secret.yaml` replace the `INSERT_CENSUS_KEY` value with the ccrpc census key, found in the passwords file.
Replace `INSERT_DB_PASSWORD` with the password for the pcd_admin user in the postgis database, this password is found in the passwords file.

Create a namespace called `census-download` in your kubernetes cluster with `microk8s.kubectl create ns census-download`.

Run `skaffold build -p import` to build the Docker image the script will use.

Run `skaffold dev -p import` to run the script.

After the script has run, to see the csv output of the script run `k rsync pvc census-api-output@census-download:/ $PWD/import/output -av`.
This will copy the output of the script into the output folder inside of the input folder.
