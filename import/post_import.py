import psycopg2
from config import DB_INFO

DROP_TRACT_INDEX = """
    DROP INDEX IF EXISTS census.ameren_tract_volatile_geoid_index;
    """

CREATE_TRACT_INDEX_GEOID = """
    CREATE UNIQUE INDEX ameren_tract_volatile_geoid_index ON census.ameren_tract_volatile (geoid);
    """


DROP_ZCTA_INDEX = """
    DROP INDEX IF EXISTS census.ameren_zcta_volatile_zcta_index;
    """

CREATE_ZCTA_INDEX_GEOID = """
    CREATE UNIQUE INDEX ameren_zcta_volatile_zcta_index ON census.ameren_zcta_volatile (zcta);
    """

DROP_SPATIAL_INDEX = """
    DROP INDEX IF EXISTS census.spatial_volatile_level_geoid_index;
    """

CREATE_SPATIAL_INDEX_LEVEL_GEOID = """
    CREATE UNIQUE INDEX spatial_volatile_level_geoid_index ON census.spatial_volatile (level,geoid);
    """


def create_spatial_index():
    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(DROP_TRACT_INDEX)

    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(CREATE_TRACT_INDEX_GEOID)

    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(DROP_ZCTA_INDEX)

    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(CREATE_ZCTA_INDEX_GEOID)

    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(DROP_SPATIAL_INDEX)

    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(CREATE_SPATIAL_INDEX_LEVEL_GEOID)


if __name__ == "__main__":
    create_spatial_index()
