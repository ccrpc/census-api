import psycopg2
from config import DB_INFO

# Create schema
CREATE_SCHEMA = "CREATE SCHEMA IF NOT EXISTS census;"


def create_schema():
    with psycopg2.connect(**DB_INFO) as conn:
        with conn.cursor() as curs:
            curs.execute(CREATE_SCHEMA)


if __name__ == '__main__':
    create_schema()
