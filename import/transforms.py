transforms = [
        (
            'diverse_percentage',
            lambda df: (
                (df.individuals - df.non_hispanic_white) / df.individuals
            )
        ),
        (
            'diverse_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.diverse_percentage
                .rank(pct=True)
            )
        ),
        (
            'iq_families_percentage',
            lambda df: (
                df.family_households_income_0_299_pl / df.family_households
            )
        ),
        (
            'iq_families_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.iq_families_percentage
                .rank(pct=True)
            )
        ),
        (
            'iq_seniors_percentage',
            lambda df: (
                df.pop_by_age_65_plus_0_299_pl / df.pop_by_age_65_plus
            )
        ),
        (
            'iq_seniors_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.iq_seniors_percentage
                .rank(pct=True)
            )
        ),
        (
            'pop_25_plus_less_than_hs_percentage',
            lambda df: (
                df.pop_25_plus_less_than_hs / df.pop_25_plus
            )
        ),
        (
            'pop_25_plus_less_than_hs_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.pop_25_plus_less_than_hs_percentage
                .rank(pct=True)
            )
        ),
        (
            'disability_percentage',
            lambda df: (
                df.disability_pop / df.disability_noninstitutionalized_pop
            )
        ),
        (
            'disability_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.disability_percentage
                .rank(pct=True)
            )
        ),
        (
            'renters_percentage',
            lambda df: (
                df.renter_occupied_units / df.occupied_housing_units
            )
        ),
        (
            'renters_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.renters_percentage
                .rank(pct=True)
            )
        ),
        (
            'seniors_percentage',
            lambda df: (
                df.pop_by_age_65_plus / df.individuals
            )
        ),
        (
            'seniors_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.seniors_percentage
                .rank(pct=True)
            )
        ),
        (
            'unemployed_percentage',
            lambda df: (
                df.unemployed_civilian / df.civilian_labor_force
            )
        ),
        (
            'unemployed_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.unemployed_percentage
                .rank(pct=True)
            )
        ),
        (
            'veteran_percentage',
            lambda df: (
                df.veterans / df.veterans_civilian_pop
            )
        ),
        (
            'veterans_percentile',
            lambda df: (
                # df.groupby(['level'])
                df.veteran_percentage
                .rank(pct=True)
            )
        )
    ]