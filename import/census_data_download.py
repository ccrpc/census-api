import numpy as np
import pandas as pd
from config import get_config, engine, CENSUS_KEY, DATASET_ENDPOINT, CENSUS_YEAR, UPLOAD_SQL
import sys
from helpers import chunks, flatten_variables, get_data


def get_census_level_data(level, variables, counties=[""]):
    template = (
        f"https://api.census.gov/data/{CENSUS_YEAR}/{DATASET_ENDPOINT}" r"?get={}" "&for={}:*&in=state:17{}&key={}"
    )
    census_level_df = pd.DataFrame()

    print(f"Downloading {level} data...")
    for county in counties:
        county_df = pd.DataFrame()

        # API variable request limit is 50, so we have to
        # send a series of requests and merge the results
        for chunk in chunks(variables, chunk_size=50):
            # Construct URL from template
            url = template.format(",".join(chunk), level.lower(), f" county:{county}" if county else "", CENSUS_KEY)

            # Call API and retrieve response then Construct DataFrame with response data
            temp_df = get_data(url)

            # Populate these values with empty strings
            # if not provided by the response
            for temp_level in ["state", "county", "tract", "block group"]:
                if temp_level not in temp_df.columns:
                    temp_df[temp_level] = ""

            # Instantiate summative DataFrame or merge if exists
            if county_df.empty:
                county_df = temp_df
            else:
                county_df = county_df.merge(temp_df, how="outer", on=["state", "county", "tract", "block group"])

        if census_level_df.empty:
            census_level_df = county_df
        else:
            census_level_df = pd.concat([census_level_df, county_df])

    # Assign the batch level column (County, Tract, or Block Group)
    census_level_df["level"] = level
    print(f"{level} download complete!")
    return census_level_df


def get_census_data(variable_sets, additional_levels=[]):
    df = pd.DataFrame()
    census_variables = flatten_variables(variable_sets)

    for level in ["County", "Tract"]:
        census_level_df = get_census_level_data(level, census_variables)

        if df.empty:
            df = census_level_df
        else:
            df = pd.concat([df, census_level_df])

    county_codes = df.county.unique()

    if "Block Group" in additional_levels:
        block_level_df = get_census_level_data("Block Group", census_variables, county_codes)
        df = pd.concat([df, block_level_df])

    # Cast all census variable columns to type float
    convert_dict = {variable: float for variable in census_variables}
    df = df.astype(convert_dict)

    # Combine variables to construct demographic columns
    new_df = pd.DataFrame()
    for set in variable_sets:
        new_df[set["label"]] = np.sum(df[set["variables"]], axis=1)

    # Construct the full geoid from parts
    new_df["geoid"] = df["state"] + df["county"] + df["tract"] + df["block group"]
    # Retain the level information
    new_df["level"] = df["level"]

    return new_df


if __name__ == "__main__":
    config_name = sys.argv[1]
    config = get_config(config_name)

    print("\nCensus Tract Data Download Started")
    df = get_census_data(config.census_demographics, config.additional_levels)

    # Some variables are either created from different variables or changed
    # post download. Here we apply those transformations.
    for label, t_func in config.transforms:
        df[label] = t_func(df)

    # Save the census data to a csv file.
    df.to_csv(f'./output/{DATASET_ENDPOINT.split("/")[-1]}_{CENSUS_YEAR}_census_data.csv', index=False)

    if UPLOAD_SQL:
        # Create and Populate table "geo_data"
        print("Uploading Census Tract Data to Database")
        df.to_sql(config.db_table, engine, schema="census", if_exists="replace")
    print("Census Tract Data Download Completed")
