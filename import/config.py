from abc import ABC
import os
import yaml
from sqlalchemy import create_engine

CENSUS_KEY = os.environ.get("CENSUS_KEY")
CENSUS_YEAR = os.environ.get("CENSUS_YEAR") or 2020
DATASET_ENDPOINT = os.environ.get("DATASET_ENDPOINT") or "acs/acs5"
DB_HOST = os.environ.get("DB_HOST")
DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")
DB_PORT = os.environ.get("DB_PORT")
AMEREN_DEMOGRAPHICS = yaml.load(os.environ.get("AMEREN_DEMOGRAPHICS"), Loader=yaml.Loader)
AMEREN_SUBJECT_DEMOGRAPHICS = yaml.load(os.environ.get("AMEREN_SUBJECT_DEMOGRAPHICS"), Loader=yaml.Loader)
UPLOAD_SQL: bool = os.environ.get("UPLOAD_SQL") == "true"

DB_INFO = {
    "host": DB_HOST,
    "port": DB_PORT,
    "dbname": DB_NAME,
    "user": DB_USER,
    "password": DB_PASSWORD,
}

engine = create_engine(f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}")


class ProjectConfig(ABC):
    pass


class AmerenConfig(ProjectConfig):
    name = "Ameren Config"
    db_table = "ameren_tract_volatile"
    additional_levels = []
    census_demographics = AMEREN_DEMOGRAPHICS
    subject_table_demographics = AMEREN_SUBJECT_DEMOGRAPHICS
    # As the percentage and percentile variables are created by the mbtiles SQL in the Ameren project,
    # They do not need to be defined here.
    # transforms = [
    #     ("diverse_percentage", lambda df: ((df.individuals - df.non_hispanic_white) / df.individuals)),
    #     ("diverse_percentile", lambda df: (df.groupby(["level"]).diverse_percentage.rank(pct=True))),
    #     ("iq_families_percentage", lambda df: (df.family_households_income_0_299_pl / df.family_households)),
    #     ("iq_families_percentile", lambda df: (df.groupby(["level"]).iq_families_percentage.rank(pct=True))),
    #     ("iq_seniors_percentage", lambda df: (df.pop_by_age_65_plus_0_299_pl / df.pop_by_age_65_plus)),
    #     ("iq_seniors_percentile", lambda df: (df.groupby(["level"]).iq_seniors_percentage.rank(pct=True))),
    #     ("pop_25_plus_less_than_hs_percentage", lambda df: (df.pop_25_plus_less_than_hs / df.pop_25_plus)),
    #     (
    #         "pop_25_plus_less_than_hs_percentile",
    #         lambda df: (df.groupby(["level"]).pop_25_plus_less_than_hs_percentage.rank(pct=True)),
    #     ),
    #     ("disability_percentage", lambda df: (df.disability_pop / df.disability_noninstitutionalized_pop)),
    #     ("disability_percentile", lambda df: (df.groupby(["level"]).disability_percentage.rank(pct=True))),
    #     ("renters_percentage", lambda df: (df.renter_occupied_units / df.occupied_housing_units)),
    #     ("renters_percentile", lambda df: (df.groupby(["level"]).renters_percentage.rank(pct=True))),
    #     ("seniors_percentage", lambda df: (df.pop_by_age_65_plus / df.individuals)),
    #     ("seniors_percentile", lambda df: (df.groupby(["level"]).seniors_percentage.rank(pct=True))),
    #     ("unemployed_percentage", lambda df: (df.unemployed_civilian / df.civilian_labor_force)),
    #     ("unemployed_percentile", lambda df: (df.groupby(["level"]).unemployed_percentage.rank(pct=True))),
    #     ("veteran_percentage", lambda df: (df.veterans / df.veterans_civilian_pop)),
    #     ("veterans_percentile", lambda df: (df.groupby(["level"]).veteran_percentage.rank(pct=True))),
    # ]
    transforms = []
    subject_transforms = [
        (
            "limited_english_households_percentage",
            lambda df: df["limited_english_households_percentage"].where(
                df["limited_english_households_percentage"] >= 0, None
            ),
        ),
        ("limited_english_households_percentage", lambda df: df["limited_english_households_percentage"] / 100),
    ]


class SierraConfig(ProjectConfig):
    name = "SIERRA Config"
    db_table = "sierra"
    # fill this with 'Block Group' if you want that info
    additional_levels = ["Block Group"]
    census_demographics = [
        ("individuals", ["B01003_001E"]),
        ("households", ["B11001_001E"]),
        ("age_0_4", ["B01001_003E", "B01001_027E"]),
        (
            "age_5_17",
            [
                "B01001_004E",
                "B01001_005E",
                "B01001_006E",
                "B01001_028E",
                "B01001_029E",
                "B01001_030E",
            ],
        ),
        (
            "age_18_24",
            [
                "B01001_007E",
                "B01001_008E",
                "B01001_009E",
                "B01001_010E",
                "B01001_031E",
                "B01001_032E",
                "B01001_033E",
                "B01001_034E",
            ],
        ),
        ("age_25_34", ["B01001_011E", "B01001_012E", "B01001_035E", "B01001_036E"]),
        ("age_35_44", ["B01001_013E", "B01001_014E", "B01001_037E", "B01001_038E"]),
        ("age_45_54", ["B01001_015E", "B01001_016E", "B01001_039E", "B01001_040E"]),
        (
            "age_55_64",
            [
                "B01001_017E",
                "B01001_018E",
                "B01001_019E",
                "B01001_041E",
                "B01001_042E",
                "B01001_043E",
            ],
        ),
        (
            "age_65_74",
            [
                "B01001_020E",
                "B01001_021E",
                "B01001_022E",
                "B01001_044E",
                "B01001_045E",
                "B01001_046E",
            ],
        ),
        (
            "age_75_plus",
            [
                "B01001_023E",
                "B01001_024E",
                "B01001_025E",
                "B01001_047E",
                "B01001_048E",
                "B01001_049E",
            ],
        ),
        ("race_white", ["B02001_002E"]),
        ("race_black", ["B02001_003E"]),
        ("race_indian", ["B02001_004E"]),
        ("race_asian", ["B02001_005E"]),
        ("race_hawaiian", ["B02001_006E"]),
        ("race_other", ["B02001_007E"]),
        ("race_multiple", ["B02001_008E"]),
        ("eth_hispanic", ["B03003_003E"]),
        ("eth_not_hispanic", ["B03003_002E"]),
        ("income_0_9999", ["B19001_002E"]),
        ("income_10000_14999", ["B19001_003E"]),
        ("income_15000_24999", ["B19001_004E", "B19001_005E"]),
        ("income_25000_34999", ["B19001_006E", "B19001_007E"]),
        ("income_35000_49999", ["B19001_008E", "B19001_009E", "B19001_010E"]),
        ("income_50000_74999", ["B19001_011E", "B19001_012E"]),
        ("income_75000_99999", ["B19001_013E"]),
        ("income_100000_149999", ["B19001_014E", "B19001_015E"]),
        ("income_150000_199999", ["B19001_016E"]),
        ("income_200000_plus", ["B19001_017E"]),
        ("house_size_1", ["B11016_010E"]),
        ("house_size_2", ["B11016_003E", "B11016_011E"]),
        ("house_size_3", ["B11016_004E", "B11016_012E"]),
        ("house_size_4", ["B11016_005E", "B11016_013E"]),
        ("house_size_5", ["B11016_006E", "B11016_014E"]),
        ("house_size_6", ["B11016_007E", "B11016_015E"]),
        ("house_size_7_plus", ["B11016_008E", "B11016_016E"]),
        ("median_income", ["B19013_001E"]),
        ("non_hispanic_white", ["B01001H_001E"]),
        ("veterans", ["B21001_002E"]),
        ("people_with_disability", ["B18101_001E"]),
        ("med_income_house_size_1", ["B19019_002E"]),
        ("med_income_house_size_2", ["B19019_003E"]),
        ("med_income_house_size_3", ["B19019_004E"]),
        ("med_income_house_size_4", ["B19019_005E"]),
        ("med_income_house_size_5", ["B19019_006E"]),
        ("med_income_house_size_6", ["B19019_007E"]),
        ("med_income_house_size_7_plus", ["B19019_008E"]),
        ("veterans_civilian_pop", ["B21001_001E"]),
        ("disability_noninstitutionalized_pop", ["B18101_001E"]),
        (
            "disability_pop",
            [
                "B18101_004E",
                "B18101_007E",
                "B18101_010E",
                "B18101_013E",
                "B18101_016E",
                "B18101_019E",
                "B18101_023E",
                "B18101_026E",
                "B18101_029E",
                "B18101_032E",
                "B18101_035E",
                "B18101_038E",
            ],
        ),
        ("pop_by_age_65_plus", ["B17024_106E", "B17024_119E"]),
        (
            "pop_by_age_65_plus_0_299_pl",
            [
                "B17024_107E",
                "B17024_108E",
                "B17024_109E",
                "B17024_110E",
                "B17024_111E",
                "B17024_112E",
                "B17024_113E",
                "B17024_114E",
                "B17024_115E",
                "B17024_120E",
                "B17024_121E",
                "B17024_122E",
                "B17024_123E",
                "B17024_124E",
                "B17024_125E",
                "B17024_126E",
                "B17024_127E",
                "B17024_128E",
            ],
        ),
        ("family_households", ["B17026_001E"]),
        (
            "family_households_income_0_299_pl",
            [
                "B17026_002E",
                "B17026_003E",
                "B17026_004E",
                "B17026_005E",
                "B17026_006E",
                "B17026_007E",
                "B17026_008E",
                "B17026_009E",
                "B17026_010E",
            ],
        ),
        ("pop_25_plus", ["B06009_001E"]),
        ("pop_25_plus_less_than_hs", ["B06009_002E"]),
        ("occupied_housing_units", ["B25003_001E"]),
        ("renter_occupied_units", ["B25003_003E"]),
        ("civilian_labor_force", ["B23025_003E"]),
        ("unemployed_civilian", ["B23025_005E"]),
    ]
    transforms = []


def get_config(project: str):
    if project == "ameren":
        return AmerenConfig()

    elif project == "sierra":
        return SierraConfig()

    else:
        raise (f'"{project}" is an invalid project configuration..."')
