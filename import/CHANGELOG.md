# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.7](https://gitlab.com/ccrpc/census-api/compare/v1.1.6...v1.1.7) (2020-11-25)


### Bug Fixes

* **import:** Add schema to the import script ([ae112fc](https://gitlab.com/ccrpc/census-api/commit/ae112fcb6ee4f70f8eff4f03667c926a695d0b21))

### [1.1.6](https://gitlab.com/ccrpc/census-api/compare/v1.1.5...v1.1.6) (2020-11-25)

### [1.1.5](https://gitlab.com/ccrpc/census-api/compare/v1.1.4...v1.1.5) (2020-11-25)

### [1.1.4](https://gitlab.com/ccrpc/census-api/compare/v1.1.3...v1.1.4) (2020-11-25)

### [1.1.1](https://gitlab.com/ccrpc/census-api/compare/v1.1.0...v1.1.1) (2020-11-25)

## [1.1.0](https://gitlab.com/ccrpc/census-api/compare/v1.0.1...v1.1.0) (2020-11-25)


### Features

* **census download scripts:** intermediate work on census download scripts ([1b099b6](https://gitlab.com/ccrpc/census-api/commit/1b099b624922edc5da21b839ee674779b3972c16))
* **census microservice:** added transformed fields ([e4fa296](https://gitlab.com/ccrpc/census-api/commit/e4fa2965b47a3b2f0c194e1baca55ef3dcf5b2dc))
* **spatial microservice:** created spatial/census download scripts ([d611cd2](https://gitlab.com/ccrpc/census-api/commit/d611cd27e704124567d143c4d4d3b5f4bc00f1cb))
