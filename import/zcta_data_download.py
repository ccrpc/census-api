import pandas as pd
from helpers import chunks, get_data, combine_data, flatten_variables
import sys
from config import get_config, engine, CENSUS_YEAR, DATASET_ENDPOINT, CENSUS_KEY, UPLOAD_SQL


def get_zip_level_data(variables, census_year, census_dataset, census_key, subject_table=False):
    template = (
        f"https://api.census.gov/data/{census_year}/{census_dataset}{'/subject' if subject_table else ''}"
        r"?get={}"
        f"&for=zip%20code%20tabulation%20area:*"
        f"&key={census_key}"
    )
    zip_level_df = pd.DataFrame()

    print("Downloading ZCTA data...")

    flat_variables = flatten_variables(variables)
    for chunk in chunks(flat_variables):
        print(f"- Downloading {chunk[0]}...{chunk[-1]}")
        url = template.format(",".join(chunk))
        temp_df = get_data(url)

        if temp_df is None:
            print("Download failed for chunk {}...{}".format(chunk[0], chunk[-1]))
            print()

        else:
            if not len(zip_level_df.columns):
                zip_level_df = temp_df
            else:
                zip_level_df = zip_level_df.merge(temp_df, how="outer", on="zip code tabulation area")

    return zip_level_df


if __name__ == "__main__":
    print("\nZip Code Tabulation Area Data Download Started")

    config_name = sys.argv[1]
    config = get_config(config_name)

    # zip_codes = yaml.load(os.environ.get('ILLINOIS_ZIP_CODES'), Loader=yaml.Loader)

    # Getting the census demographic data for the base table
    raw_df = get_zip_level_data(
        config.census_demographics, CENSUS_YEAR, DATASET_ENDPOINT, CENSUS_KEY, subject_table=False
    )
    print("Saving raw Zip Code Tabulation Area base table data to CSV")
    raw_df.to_csv(f'./output/{DATASET_ENDPOINT.split("/")[-1]}_{CENSUS_YEAR}_zcta_base_raw.csv', index=False)

    # Getting census demographic data from the subject table
    raw_subject_df = get_zip_level_data(
        config.subject_table_demographics, CENSUS_YEAR, DATASET_ENDPOINT, CENSUS_KEY, subject_table=True
    )
    print("Saving raw Zip Code Tabulation Area subject table data to CSV")
    raw_subject_df.to_csv(f'./output/{DATASET_ENDPOINT.split("/")[-1]}_{CENSUS_YEAR}_zcta_subject_raw.csv', index=False)

    # Creating the variables by adding the underlying data together
    df = combine_data(raw_df, config.census_demographics, ["zip code tabulation area"])
    for col, tf in config.transforms:
        df[col] = tf(df)

    subject_df = combine_data(raw_subject_df, config.subject_table_demographics, ["zip code tabulation area"])
    for col, tf in config.subject_transforms:
        subject_df[col] = tf(subject_df)

    df = pd.merge(df, subject_df, how="left", on="zip code tabulation area")

    df = df.rename(columns={"zip code tabulation area": "zcta"})

    print("Saving Zip Code Tabulation Area Data to csv file")
    df.to_csv(f'./output/{DATASET_ENDPOINT.split("/")[-1]}_{CENSUS_YEAR}_zcta.csv', index=False)

    if UPLOAD_SQL:
        print("Uploading Zip Code Tabulation Area Data to Database")
        df.to_sql("ameren_zcta_volatile", engine, schema="census", if_exists="replace")

    print("Zip Code Tabulation Area Download Completed\n")
