import itertools as it
import requests
import numpy as np
import pandas as pd


# Helpers
def chunks(chunk, chunk_size=50):
    if len(chunk) > chunk_size:
        return (chunk[:chunk_size], *chunks(chunk[chunk_size:], chunk_size))
    else:
        return [chunk]


def flatten_variables(variable_sets):
    return list(it.chain.from_iterable(map(lambda d: d["variables"], variable_sets)))


def get_data(url):
    # Call API and retrieve response
    try:
        resp = requests.get(url)
        resp.raise_for_status()
    except Exception as e:
        print("Download failed...\n URL: {} \n {}".format(url or None, e))
        return None
    data = resp.json()

    # Construct DataFrame with response data
    temp_df = pd.DataFrame(data[1:], columns=data[0])

    return temp_df


def combine_data(df, variables, additional_columns):
    new_df = pd.DataFrame()

    df = df.apply(pd.to_numeric, errors="ignore")

    for ac in additional_columns:
        new_df[ac] = df[ac]

    for v in variables:
        new_df[v["label"]] = np.sum(df[v["variables"]], axis=1)

    return new_df
