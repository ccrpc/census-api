#!/bin/bash

export UPLOAD_SQL=true

# Note: NEVER check secrets into repository

echo "Import process started"

# Run pre import script to prep the database
echo "Running Pre Import Sripts"
python3 /opt/code/census-download/import/pre_import.py

echo "Running Download Scripts"
python3 /opt/code/census-download/import/census_data_download.py ameren
python3 /opt/code/census-download/import/zcta_data_download.py ameren
python3 /opt/code/census-download/import/spatial_data_download.py
# python /import/census_data_download.py sierra

echo "Running Post Import Script"
# We do not need to create indecies if we did not upload anything.
if [ "$UPLOAD_SQL" = true ];
then
    # Run post import script to create indexes and set up keys
    python3 /opt/code/census-download/import/post_import.py
fi

echo "Import Finished"