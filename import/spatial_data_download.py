import requests
import geopandas as gpd
from config import engine, UPLOAD_SQL
from shapely.geometry import Polygon, MultiPolygon


def iterativeDownload(errors, geo_data, template, start, end, stepSize, census_levels, logMessage):
    # Here we are downloading the census data via a loop
    # Due to limitations in the tigerweb REST api we download them in chunks as defined by stepSize and end
    for startIndex in range(start, end, stepSize):
        endIndex = startIndex + stepSize

        # If we would go over the end of the range, go just one past it instead
        if endIndex > end:
            endIndex = end + 1

        print(logMessage.format(startIndex, endIndex))

        for label, level_id in census_levels:
            census_url = template.format(level_id, startIndex, endIndex)

            # This is the url for our REST Tigerweb request for this chunk
            try:
                print(f"Downloading {label} spatial data...")

                # Here we are making the GET request to the tigerweb api
                response = requests.get(census_url)
                response.raise_for_status()

                # The information we want is in the features part of the response
                features = response.json()["features"]
                print("Number of Features in this Download: {}".format(len(features)))

                # Here we add the downloaded data to our dictionary
                geo_data["geoid"] += list(map(lambda feat: feat["attributes"]["GEOID"], features))
                geo_data["name"] += list(map(lambda feat: feat["attributes"]["NAME"], features))
                geo_data["geom"] += list(
                    map(lambda feat: MultiPolygon(map(Polygon, feat["geometry"]["rings"])), features)
                )
                geo_data["level"] += [label for _ in range(len(features))]

                print(f"{label} spatial download complete!")

            except Exception as e:
                # If there is an error with the request, print the error and also record that there was one.
                print(label, "download failed...\n", census_url, e)
                errors[label] += 1
                continue
        print()


def download_spatial_data():
    # These are the non-zcta levels for which we are downloading spatial data
    # The first entry in each pair is the name, the second is the variable number as given by tigerweb
    census_levels = (("County", 78), ("Tract", 6), ("Block Group", 8))

    # This is the template for the tigerweb request we are using for non zcta data
    # The format is "download the data where state= 17 (which is illinois) and the counties are between a given range"
    # Other notable things about the query is that we are specifying the OUTFIELDS to get the geoid and name,
    # And the SRID as 3435, which is what we use for our maps internally.
    census_template = (
        "https://tigerweb.geo.census.gov/arcgis/rest/services/TIGERweb"
        "/tigerWMS_ACS2022/MapServer/{}"
        "/query?where=STATE%3D'17'+AND+COUNTY%3E%3D'{:03}'+AND+COUNTY%3C'{:03}'&outFields=GEOID,NAME&outSR=3435&f=json"
    )

    # These are the zcta levels we are downloading, in the same format as the census_levels above
    zcta_levels = (("ZCTA", 0),)

    # To get zctas we need to use a different tigerweb query
    # The format for this one is "download the data where the zip code is between a given range"
    # Other notable things about the query is that we are specifying the OUTFIELDS to get the geoid and name,
    # And the SRID as 3435, which is what we use for our maps internally.
    zcta_template = (
        "https://tigerweb.geo.census.gov/arcgis/rest/services/TIGERweb"
        "/tigerWMS_ACS2022/MapServer/{}"
        "/query?where=GEOID%3E%3D'{:05}'+AND+GEOID%3C'{:05}'&outFields=GEOID,NAME&outSR=3435&f=json"
    )

    # Here we initialize the dictionary we are going to use to hold the spatial data
    geo_data = {
        "geoid": [],
        "name": [],
        "geom": [],
        "level": [],
    }

    # Here is an array to keep track of any errors we may run into during the download
    errors = {label[0]: 0 for label in census_levels + zcta_levels}

    # There are 203 counties in illinois
    # Due to a limitation in Tigerweb in how much data it can return in one go, we download them in chunks of 50
    iterativeDownload(
        errors=errors,
        geo_data=geo_data,
        template=census_template,
        start=0,
        end=203,
        stepSize=50,
        census_levels=census_levels,
        logMessage="Downloading Data for counties {} <= county < {}",
    )
    print("===\n")

    # Now we download the zip code data, Illinois has zip codes >= 60000 and < 63000
    # We face the same restriction of the tigerweb api limiting how many results can be returned
    # But for the zip codes we can use chunks of 1000
    iterativeDownload(
        errors=errors,
        geo_data=geo_data,
        template=zcta_template,
        start=60000,
        end=63000,
        stepSize=1000,
        census_levels=zcta_levels,
        logMessage="Downloading Data for zip codes {} <= zcta < {}",
    )
    print("===\n")

    print("Errors: {}".format(errors))

    # Convert our dictionary to a GeoDataFrame and return it.
    return gpd.GeoDataFrame(geo_data, crs="EPSG:3435", geometry="geom")


if __name__ == "__main__":
    print("\nSpatial Data Download Started")

    gdf = download_spatial_data()
    gdf = gdf.set_crs(3435, allow_override=True)

    print("Writing spatial data to geopackage file")

    # Save the GeoDataFrame to a file in the output folder
    gdf.to_file("output/spatial_illinois.gpkg", driver="GPKG", mode="w")

    if UPLOAD_SQL:
        # Create and Populate table "geo_data"
        print("Uploading spatial data to database")
        gdf.to_postgis("spatial_volatile", engine, schema="census", if_exists="replace")

    print("Spatial Data Download Completed\n")
